<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Help extends Model
{
    public $timestamps = false;

    public function lov(){
        return Help::all();
    }

    public function show($id){
        return Help::find($id);
    }
}
