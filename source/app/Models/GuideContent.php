<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GuideContent extends Model
{
    protected $table = 'guide_contents';
    public $timestamps = false;

    public function insert($data){
        return GuideContent::insert($data);
    }

    public function lov(){
        return GuideContent::all();
    }

    public function show($id){
        return GuideContent::find($id);
    }

    public function lovSubTitleGuideContent(){
        return GuideContent::where("type", "st")
                ->get();
    }

    public function lovTitleGuideContentsJson(){
        return GuideContent::where("type", 'tt')
                ->get()->toJson();
    }

    public function lovSubTitleGuideContentsJson($id){
        return GuideContent::where("guide_contents.type", "st")
                ->where("guide_contents.guide_contents_id", $id)
                ->join("guide_contents AS gd", "guide_contents.guide_contents_id", "=", "gd.id")
                ->select("guide_contents.*", "gd.title_guide_content as sub_guide_content")
                ->get()->toJson();
    }

}
