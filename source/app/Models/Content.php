<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    public $timestamps = false;

    public function lov(){
        return Content::all();
    }

    public function show($id){
        return Content::find($id);
    }

    public function lovContentOfSubGuide($id){
        return Content::where("contents.guide_contents_id", $id)
                ->join('guide_contents as gc', 'gc.id', '=', 'contents.guide_contents_id')
                ->where('gc.id', '=', $id)
                ->select('contents.id', 'contents.title_content', 'gc.title_guide_content as sub_guide_content')
                ->get()
                ->toJson();
    }

    public function showJson($id){
        return Content::find($id)->toJson();
    }
}
