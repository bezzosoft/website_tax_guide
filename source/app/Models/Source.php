<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    public $timestamps = false;

    public function lov(){
        return Source::all();
    }

    public function show($id){
        return Source::find($id);
    }
}
