<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    public $timestamps = false;

    public function lov(){
        return Bookmark::all();
    }

    public function show($id){
        return Bookmark::find($id);
    }
}
