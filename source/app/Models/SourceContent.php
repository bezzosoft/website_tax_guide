<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SourceContent extends Model
{
    public $timestamps = false;

    public function lov(){
        return SourceContent::all();
    }

    public function show($id){
        return SourceContent::find($id);
    }
}
