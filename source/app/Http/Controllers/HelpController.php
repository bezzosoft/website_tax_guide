<?php

namespace App\Http\Controllers;

use App\Models\Help;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class HelpController extends Controller
{
    private $help;

    public function __construct()
    {
        $this->help = new Help();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["helps"] = $this->help->lov();
        return view("page/help/manage-help", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("page/help/create-help");
    }

    public function manage(){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $help = new Help();
        $help->title_help = $request->input("title_help");
        $help->value_help = $request->input("value_help");
        $help->save();

        return Redirect::to("manage-help");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["help"] = $this->help->show($id);
        return view("page/help/edit-help", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $help = $this->help->show($id);
        $help->title_help = $request->input("title_help");
        $help->value_help = $request->input("value_help");
        $help->save();

        return Redirect::to("manage-help");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $help = $this->help->show($id);
        $help->delete();

        return Redirect::to("manage-help");
    }
}
