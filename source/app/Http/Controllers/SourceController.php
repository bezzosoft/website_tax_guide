<?php

namespace App\Http\Controllers;

use App\Models\Content;
use App\Models\Source;
use App\Models\SourceContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SourceController extends Controller
{
    private $source;
    private $source_content;
    private $content;

    public function __construct()
    {
        $this->source = new Source();
        $this->source_content = new SourceContent();
        $this->content = new Content();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["sources"] = $this->source->lov();
        $data["source_contents"] = $this->source_content->lov();
        return view("page/source/manage-source", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["sources"] = $this->source->lov();
        $data["contents"] = $this->content->lov();
        return view("page/source/create-source", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $source = new Source();
        $source->name_source = $request->input("name_source");
        $type_source = $request->input("type");
        $source->type = $type_source;

        if ($request->hasFile("link_source")){
            if ($request->file("link_source")){
                $source->link_source = $request->file("link_source")->getClientOriginalName();
                if ($type_source == 'i'){
                    $request->file("link_source")->move("images/sources", $source->link_source);
                }
                else if ($type_source == 'f'){
                    $request->file("link_source")->move("files", $source->link_source);
                }
                $source->save();

                return Redirect::to("manage-source");
            }
            else{
//                echo "<script>alert('tidak terdeteksi');</script>";
                return Redirect::back();
            }
        }
        else{
//            echo "<script>alert('Bukan file');</script>";
            return Redirect::back();
        }
    }

    public function storeOfContent(Request $request)
    {
        $source_content = new SourceContent();
        $source_content->source_id = $request->input("source_id");
        $source_content->content_id = $request->input("content_id");

        $source_content->save();

        return Redirect::to("manage-source");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["source"] = $this->source->show($id);
        return view("page/source/edit-source", $data);
    }

    public function editContent($id){
        $data["source_content"] = $this->source_content->show($id);
        $data["sources"] = $this->source->lov();
        $data["contents"] = $this->content->lov();
        return view("page/source/edit-source-content", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $source = $this->source->show($id);
        $source->name_source = $request->input("name_source");
        $source->type = $request->input("type");

        $source->save();
        return Redirect::to("manage-source");
    }

    public function updateContent(Request $request, $id){
        $source_content = $this->source_content->show($id);
        $source_content->source_id = $request->input("source_id");
        $source_content->content_id = $request->input("content_id");
        $source_content->save();

        return Redirect::to("manage-source");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $source = $this->source->show($id);
        unlink("images/sources/$source->link_source");
        $source->delete();

        return Redirect::to("manage-source");
    }
}
