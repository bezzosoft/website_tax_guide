<?php

namespace App\Http\Controllers;

use App\Models\Content;
use App\Models\GuideContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ContentController extends Controller
{
    private $guide_content;
    private $content;

    public function __construct()
    {
        $this->guide_content = new GuideContent();
        $this->content = new Content();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["contents"] = $this->content->lov();
        return view("page/content/manage-content", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["guide_contents"] = $this->guide_content->lovSubTitleGuideContent();
        return view("page/content/create-content", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $content = new Content();
        $content->value_content = $request->input("value_content");
        $content->title_content = $request->input("title_content");
        $content->guide_contents_id = $request->input("guide_contents_id");

        $content->save();
        return Redirect::to("manage-content");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["content"] = $this->content->show($id);
        $data["guide_contents"] = $this->guide_content->lovSubTitleGuideContent();
        return view("page/content/edit-content", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $content = $this->content->show($id);
        $content->title_content = $request->input("title_content");
        $content->value_content = $request->input("value_content");
        $content->guide_contents_id = $request->input("guide_contents_id");

        $content->save();
        return Redirect::to("manage-content");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $content = $this->content->show($id);
        $content->delete();

        return Redirect::to("manage-content");
    }

    public function lovContentOfSubGuide($id){
        return $this->content->lovContentOfSubGuide($id);
    }

    public function showJson($id){
        return $this->content->showJson($id);
    }
}
