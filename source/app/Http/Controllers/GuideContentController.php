<?php

namespace App\Http\Controllers;

use App\Models\GuideContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rules\In;

class GuideContentController extends Controller
{
    private $guide_content;

    public function __construct()
    {
        $this->guide_content = new GuideContent();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["guide_contents"] = $this->guide_content->lov();
        return view("page/guide_content/manage-guide-content", $data);
    }

    public function indexJson($type){
        if ($type == 'title'){
            return $this->guide_content->lovTitleGuideContentsJson();
        }
        else if($type == 'subtitle'){
            return $this->guide_content->lovSubTitleGuideContentsJson();
        }
        else{
            return '{ "status": 404 }';
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["guide_contents"] = $this->guide_content->lov();
        return view("page/guide_content/create-guide-content", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $guide_contents = new GuideContent();
        $guide_contents->title_guide_content = $request->input("title_guide_content");
        $guide_contents->type = $request->input("type");
        $guide_contents->guide_contents_id = $request->input("guide_contents_id");

        if ($guide_contents->guide_contents_id == 0){
            $guide_contents->guide_contents_id = null;
        }

        $guide_contents->save();

        return Redirect::to('manage-guide-content');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["guide_contents"] = $this->guide_content->lov();
        $data["guide_content"] = $this->guide_content->show($id);
        return view("page/guide_content/edit-guide-content", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $guide_contents = $this->guide_content->show($id);
        $guide_contents->title_guide_content = $request->input("title_guide_content");
        $guide_contents->type = $request->input("type");
        $guide_contents->guide_contents_id = $request->input("guide_contents_id");

        if ($guide_contents->guide_contents_id == 0){
            $guide_contents->guide_contents_id = null;
        }

        $guide_contents->save();

        return Redirect::to('manage-guide-content');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $guide_contents = $this->guide_content->show($id);
        $guide_contents->delete();

        return Redirect::to('manage-guide-content');
    }

    public function lovSubtitleGuideContents($id){
        return $this->guide_content->lovSubTitleGuideContentsJson($id);
    }
}
