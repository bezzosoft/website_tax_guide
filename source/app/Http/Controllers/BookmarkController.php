<?php

namespace App\Http\Controllers;

use App\Models\Bookmark;
use App\Models\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class BookmarkController extends Controller
{
    private $bookmark, $content;

    public function __construct()
    {
        $this->bookmark = new Bookmark();
        $this->content = new Content();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["bookmarks"] = $this->bookmark->lov();
        return view("page/bookmark/manage-bookmark", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["contents"] = $this->content->lov();
        return view("page/bookmark/create-bookmark", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bookmark = new Bookmark();
        $bookmark->description = $request->input('description');
        $bookmark->content_id = $request->input("content_id");
        $bookmark->save();

        return Redirect::to('manage-bookmark');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["contents"] = $this->content->lov();
        $data["bookmark"] = $this->bookmark->show($id);
        return view("page/bookmark/edit-bookmark", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bookmark = $this->bookmark->show($id);
        $bookmark->description = $request->input("description");
        $bookmark->content_id = $request->input("content_id");
        $bookmark->save();

        return Redirect::to('manage-bookmark');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bookmark = $this->bookmark->show($id);
        $bookmark->delete();

        return Redirect::to('manage-bookmark');
    }
}
