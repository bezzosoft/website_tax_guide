@extends('index')

@section('title', 'TaxGuide | Guide Content')

@section('style')
  <!-- Datatables -->
  <link href="{{ url("vendors/datatables.net-bs/css/dataTables.bootstrap.min.css") }}" rel="stylesheet">
  <link href="{{ url("vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css") }}" rel="stylesheet">
  <link href="{{ url("vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css") }}" rel="stylesheet">
  <link href="{{ url("vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css") }}" rel="stylesheet">
  <link href="{{ url("vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css") }}" rel="stylesheet">
@endsection

@section('content')


  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>List Source of Content</h3>
        </div>
      </div>

      <div class="clearfix"></div>

      <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Source of Content</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable-buttons" class="table table-striped table-bordered">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Name Source</th>
                  <th>Source</th>
                  <th>Action</th>
                </tr>
                </thead>


                <tbody>
                @foreach($sources as $source)
                  <tr>
                    <td>{{ $source->id }}</td>
                    <td>{{ $source->name_source }}</td>
                    <td>
                      @if($source->type == 'i')
                        <img class="img-responsive" src="{{ asset("images/sources/$source->link_source") }}" alt="">
                      @elseif($source->type == 'f')
                        <a target="_blank" href="{{ asset("files/$source->link_source") }}">{{ $source->name_source }}</a>
                      @endif
                    </td>
                    <td>
                      <a class="btn btn-link" href="{{ url("edit-source/{$source->id}") }}">
                        <i class="fa fa-pencil"></i> Edit</a>
                      <form action="{{ url("delete-source/{$source->id}") }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">

                        <button class="btn btn-link">
                          <i class="fa fa-trash"></i> Delete</button>
                      </form>
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>List of Source</h3>
        </div>
      </div>

      <div class="clearfix"></div>

      <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Source</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable-buttons" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Source</th>
                    <th>Content</th>
                    <th>Action</th>
                  </tr>
                </thead>


                <tbody>
                @foreach($source_contents as $source_content)
                  <tr>
                    <td>{{ $source_content->id }}</td>
                    <td>
                      @php
                        $sources_model = new \App\Models\Source();
                        $source_of_sourcecontent = $sources_model->show($source_content->source_id);
                      @endphp
                      {{ $source_of_sourcecontent->name_source }}
                    </td>
                    <td>
                      @php
                        $content_model = new \App\Models\Content();
                        $content = $content_model->show($source_content->content_id);
                      @endphp
                      {{ $content->title_content }}
                    </td>
                    <td>
                      <a class="btn btn-link" href="{{ url("edit-source-content/{$source_content->id}") }}">
                        <i class="fa fa-pencil"></i> Edit</a>
                      <form action="{{ url("delete-source-content/{$source_content->id}") }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">

                        <button class="btn btn-link">
                          <i class="fa fa-trash"></i> Delete</button>
                      </form>
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- /page content -->
@endsection

@section('script')
  <!-- FastClick -->
  <script src="{{ asset("vendors/fastclick/lib/fastclick.js") }}"></script>
  <!-- NProgress -->
  <script src="{{ asset("vendors/nprogress/nprogress.js") }}"></script>
  <!-- Datatables -->
  <script src="{{ asset("vendors/datatables.net/js/jquery.dataTables.min.js") }}"></script>
  <script src="{{ asset("vendors/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
  <script src="{{ asset("vendors/datatables.net-buttons/js/dataTables.buttons.min.js") }}"></script>
  <script src="{{ asset("vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js") }}"></script>
  <script src="{{ asset("vendors/datatables.net-buttons/js/buttons.flash.min.js") }}"></script>
  <script src="{{ asset("vendors/datatables.net-buttons/js/buttons.html5.min.js") }}"></script>
  <script src="{{ asset("vendors/datatables.net-buttons/js/buttons.print.min.js") }}"></script>
  <script src="{{ asset("vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js") }}"></script>
  <script src="{{ asset("vendors/datatables.net-keytable/js/dataTables.keyTable.min.js") }}"></script>
  <script src="{{ asset("vendors/datatables.net-responsive/js/dataTables.responsive.min.js") }}"></script>
  <script src="{{ asset("vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js") }}"></script>
  <script src="{{ asset("vendors/datatables.net-scroller/js/datatables.scroller.min.js") }}"></script>
  <script src="{{ asset("vendors/jszip/dist/jszip.min.js") }}"></script>
  <script src="{{ asset("vendors/pdfmake/build/pdfmake.min.js") }}"></script>
  <script src="{{ asset("vendors/pdfmake/build/vfs_fonts.js") }}"></script>

  <!-- Datatables -->
  <script>
    $(document).ready(function() {
      var handleDataTableButtons = function() {
        if ($("#datatable-buttons").length) {
          $("#datatable-buttons").DataTable({
            dom: "Bfrtip",
            buttons: [
              {
                extend: "copy",
                className: "btn-sm"
              },
              {
                extend: "csv",
                className: "btn-sm"
              },
              {
                extend: "excel",
                className: "btn-sm"
              },
              {
                extend: "pdfHtml5",
                className: "btn-sm"
              },
              {
                extend: "print",
                className: "btn-sm"
              },
            ],
            responsive: true
          });
        }
      };

      TableManageButtons = function() {
        "use strict";
        return {
          init: function() {
            handleDataTableButtons();
          }
        };
      }();

      $('#datatable').dataTable();
      $('#datatable-keytable').DataTable({
        keys: true
      });

      $('#datatable-responsive').DataTable();

      $('#datatable-scroller').DataTable({
        ajax: "js/datatables/json/scroller-demo.json",
        deferRender: true,
        scrollY: 380,
        scrollCollapse: true,
        scroller: true
      });

      var table = $('#datatable-fixed-header').DataTable({
        fixedHeader: true
      });

      TableManageButtons.init();
    });
  </script>
  <!-- /Datatables -->
  @endsection