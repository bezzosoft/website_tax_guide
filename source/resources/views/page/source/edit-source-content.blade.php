@extends('index')

@section('title', 'TaxGuide | Guide Content')

@section('content')
  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Create Source of Content</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Source of Content</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"
                    method="post" action="{{ url("update-source-content/{$source_content->id}") }}">

                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
                <div class="form-group">
                  <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Source : </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="source_id" class="form-control col-md7 col-xs-12">
                      @php
                        $source_model = new \App\Models\Source();
                        $source_choosed = $source_model->show($source_content->source_id);
                      @endphp
                      <option value="{{ $source_content->source_id }}">{{ $source_choosed->name_source }}</option>
                      <option disabled> -- </option>
                      @foreach($sources as $source)
                        <option value="{{ $source->id }}">{{ $source->name_source }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Content : </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="content_id" class="form-control col-md7 col-xs-12">
                      @php
                        $content_model = new \App\Models\Content();
                        $content_choosed = $content_model->show($source_content->content_id);
                      @endphp
                      <option value="{{ $source_content->content_id }}">{{ $content_choosed->title_content }}</option>
                      <option disabled> -- </option>
                      @foreach($contents as $content)
                        <option value="{{ $content->id }}">{{ $content->title_content }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-success">Edit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->
@endsection