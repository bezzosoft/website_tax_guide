@extends('index')

@section('title', 'TaxGuide | Guide Content')

@section('content')
  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Create Guide Content</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Data Guide Content</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form data-parsley-validate class="form-horizontal form-label-left"
                    action="{{ url("insert-guide-content")  }}" method="POST" accept-charset="UTF-8">

                {{ csrf_field() }}

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title-guide-content">Title Guide Content
                    <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="title_guide_content" id="title-guide-content" required="required"
                           class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="form-group">
                  <label for="sub-guide-content" class="control-label col-md-3 col-sm-3 col-xs-12">Type Guide Content : </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="type" class="form-control col-md7 col-xs-12">
                      <option value="tt">Title Guide</option>
                      <option value="st">Sub Title Guide</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="sub-guide-content" class="control-label col-md-3 col-sm-3 col-xs-12">Sub Guide From : </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="guide_contents_id" class="form-control col-md7 col-xs-12">
                      <option value="0"> Pilih </option>
                      @foreach($guide_contents as $guide_content)
                        <option value="{{ $guide_content->id }}">{{ $guide_content->title_guide_content }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-success">Create</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->
  @endsection