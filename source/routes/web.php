<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("login");
});

Auth::routes();

Route::get('/home', 'AuthenticationController@index');

//Guide Content
Route::get('/create-guide-content', 'GuideContentController@create');
Route::post('/insert-guide-content', 'GuideContentController@store');
Route::get('/manage-guide-content', 'GuideContentController@index');
Route::get('/edit-guide-content/{id}', 'GuideContentController@edit');
Route::put('/update-guide-content/{id}', 'GuideContentController@update');
Route::delete('/delete-guide-content/{id}', 'GuideContentController@destroy');

//Json
Route::get('/get-guide-contents/{type}', 'GuideContentController@indexJson');
Route::get('/get-sub-guide-contents/{id}', 'GuideContentController@lovSubtitleGuideContents');

//Content
Route::get('/create-content', 'ContentController@create');
Route::post('/insert-content', 'ContentController@store');
Route::get('/edit-content/{id}', 'ContentController@edit');
Route::put('/update-content/{id}', 'ContentController@update');
Route::delete('/delete-content/{id}', 'ContentController@destroy');
Route::get('/manage-content', 'ContentController@index');

Route::get('/get-content-subguide/{id}', 'ContentController@lovContentOfSubGuide');
Route::get('/get-content/{id}', 'ContentController@showJson');

//Source
Route::get('/create-source', 'SourceController@create');
Route::get('/edit-source/{id}', 'SourceController@edit');
Route::put('/update-source/{id}', 'SourceController@update');
Route::delete('/delete-source/{id}', 'SourceController@destroy');
Route::get('/manage-source', 'SourceController@index');
Route::post('/insert-source', 'SourceController@store');

Route::post('/insert-source-content', 'SourceController@storeOfContent');
Route::get('/edit-source-content/{id}', 'SourceController@editContent');
Route::put('/update-source-content/{id}', 'SourceController@updateContent');

//Bookmark
Route::get('/create-bookmark', 'BookmarkController@create');
Route::post('/insert-bookmark', 'BookmarkController@store');
Route::get('/edit-bookmark/{id}', 'BookmarkController@edit');
Route::put('/update-bookmark/{id}', 'BookmarkController@update');
Route::delete('/delete-bookmark/{id}', 'BookmarkController@destroy');
Route::get('/manage-bookmark', 'BookmarkController@index');

//Help
Route::get('/create-help', 'HelpController@create');
Route::post('/insert-help', 'HelpController@store');
Route::get('/edit-help/{id}', 'HelpController@edit');
Route::put('/update-help/{id}', 'HelpController@update');
Route::delete('/delete-help/{id}', 'HelpController@destroy');
Route::get('/manage-help', 'HelpController@index');